'use strict';
import 'reflect-metadata';
import express from 'express';

import logger from './src/util/logger';

import loaders from './src/loaders';


const port = 8010;

async function startServer(): Promise<void> {
    const app = express();

    await loaders(app);

    app.listen(port, () => logger.info(`
    ################################################
    🛡️  Server listening on port: ${port} 🛡️ 
    ################################################`)
    );
}

try {
    startServer();
}catch(err) {
    logger.error('🔥 Error while starting server: %o', err);
}