import Container, { Service, Inject } from 'typedi';

import { IRideService } from '../interfaces/IRideService';
import { IRide } from '../interfaces/IRide';
import RideRepository from '../repositories/ride';
import winston from 'winston';
import { IRideRepository } from '../interfaces/IRideRepository';
import { PagingQueryParams } from '../interfaces/IPagingParams';

@Service()
export default class RideService implements IRideService {
    @Inject('Logger') private logger: winston.Logger;
    private rideRepoInstance: IRideRepository = Container.get(RideRepository);

    createRide(rideData: IRide): Promise<IRide[]> {
        this.logger.info('RideService: createRide()');
        this.logger.debug('RideService: createRide() -> rideData: %o', rideData);
        const values = Object.values(rideData);

        return this.rideRepoInstance.saveRide(values);
    }

    async getRides(pagingParams: PagingQueryParams): Promise<IRide[]> {
        this.logger.info('RideService: getRides()');
        const { pageNumber = null, qtyPerPage= null } = pagingParams;

        if (pageNumber && qtyPerPage) {
            this.logger.debug('RideService: getRides() -> pagingParams: %o', pagingParams);
            const limit = parseInt(qtyPerPage, 10);
            const offset = limit * (parseInt(pageNumber, 10) - 1);
            return this.rideRepoInstance.getRides(limit, offset);
        }

        return this.rideRepoInstance.getRides();
    }

    getRideById(id: string): Promise<IRide[]> {
        this.logger.info('RideService: getRideById()');
        this.logger.debug('RideService: getRideById() -> id: %o', id);
        return this.rideRepoInstance.getRideById(id);
    }
}