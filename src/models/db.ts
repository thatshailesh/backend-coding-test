import { Database, RunResult } from 'sqlite3';

export default {
    dbRun:  (db: Database, query: string): Promise<RunResult> => {
        return new Promise((resolve, reject) => {
            db.run(query, function (err) {
                if (err) {
                    reject(err);
                }else {
                    resolve(this);
                }
            });
        });
    },
    dbAll: (db: Database, query: string): Promise<any[]> => {
    
        return new Promise((resolve, reject) => {
            db.all(query, (err, rows) => {
                if (err) {
                    reject(err);
                }else {
                    resolve(rows);
                }
            });
        });
    }
};
