'use strict';
import { Database } from 'sqlite3';
import dbModel from './db';

export const createRideTableSchema = async (db: Database): Promise<Database> => {
    const createTableQuery = `
    CREATE TABLE Rides
    (
    rideID INTEGER PRIMARY KEY AUTOINCREMENT,
    startLat DECIMAL NOT NULL,
    startLong DECIMAL NOT NULL,
    endLat DECIMAL NOT NULL,
    endLong DECIMAL NOT NULL,
    riderName TEXT NOT NULL,
    driverName TEXT NOT NULL,
    driverVehicle TEXT NOT NULL,
    created DATETIME default CURRENT_TIMESTAMP
    )`;

    await dbModel.dbRun(db, createTableQuery);

    return db;
};

