import LoggerInstance from '../util/logger';
import Identifiers from '../constants/identifiers';
import { Database } from 'sqlite3';
import { Container } from 'typedi';


export default (db: Database): Container => {
    Container.set(Identifiers.Logger, LoggerInstance);

    Container.set(Identifiers.Db, db);

    LoggerInstance.info('✌️ loaders injected into container');

    return Container;
};