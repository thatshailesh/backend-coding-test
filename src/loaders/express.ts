import express, { Request, Response, NextFunction } from 'express';
import bodyParser from 'body-parser';
import auth from 'express-basic-auth';
import config from 'config';
import routes from '../api';

interface Auth {
    username: string;
    password: string;
}
export default (app: express.Application): void => {

    // API Health
    app.get('/health', (req: Request, res: Response) => res.send('Healthy'));

    // Middleware that transforms the raw string of req.body into json
    app.use(bodyParser.json());

    const userAuth: any = {};
    const data: Auth = config.get('auth');

    userAuth[data.username] = data.password;
    app.use(auth({
        users: userAuth
    }));

    // Load API routes
    app.use('/', routes());

    /// catch 404 and forward to error handler
    app.use((req: Request, res: Response, next: NextFunction) => {
        const err = new Error('Not Found');
        next({status: 404, message: err.message});
    });

    app.use((err: any, req: Request, res: Response) => {
        res.status(err.status || 500);
        res.json({
            errors: {
                message: err.message,
            },
        });
    });
};