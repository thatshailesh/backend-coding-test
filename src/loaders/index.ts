import express from 'express';
import dependencyInjector from './dependencyInjector';
import expressLoader from './express';
import dbLoader from './sqlite';
import loggerInstance from '../util/logger';

export default async (app: express.Application): Promise<express.Application> => {
    const db = await dbLoader();
    loggerInstance.info('✌️ Database loaded');


    dependencyInjector(db);

    loggerInstance.info('✌️ Dependency Inject or loaded');


    expressLoader(app);

    loggerInstance.info('✌️ Express loaded');

    return app;
}; 