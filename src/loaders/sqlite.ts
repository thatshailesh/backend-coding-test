import { verbose, Database } from 'sqlite3';
import { createRideTableSchema } from '../models/schemas';
import loggerInstance from '../util/logger';

export default (): Database => {
    const sqlite3 = verbose();

    const db = new sqlite3.Database(':memory:');

    db.serialize(async () => {
        await createRideTableSchema(db);
        loggerInstance.info('✌️ Schemas created');
    });
    return db;
};