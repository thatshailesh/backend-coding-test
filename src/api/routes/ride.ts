
import { Router, Request, Response }  from 'express';
import validateRide from '../middlewares/validateRide';
import { Container } from 'typedi';
import winston from 'winston';
import RideService from '../../services/ride';
import { IRideService } from '../../interfaces/IRideService';
import validatePaging from '../middlewares/validatePaging';
import { PagingQueryParams } from '../../interfaces/IPagingParams';

const route = Router();

export default (app: Router): void => {
    app.use('/rides', route);
    const logger: winston.Logger = Container.get('Logger');
    const rideServiceInstance: IRideService = Container.get(RideService);

    route.get('/:id', async (req: Request, res: Response) => {
        try {
            const result = await rideServiceInstance.getRideById(req.params.id);

            if (!result.length) {
                return res.send({
                    errorCode: 'RIDES_NOT_FOUND_ERROR',
                    message: 'Could not find any rides'
                });
            }
            return res.send(result);
        }catch(err) {
            logger.error('🔥 Error: %o', err);
            return res.send({
                errorCode: 'SERVER_ERROR',
                message: 'Unknown error'
            });
        } 

    });

    route.post('/', validateRide, async (req: Request, res: Response) => {        
        logger.debug('Calling Create ride endpoint with body: %o', req.body );
        try {
            
            const startLat = Number(req.body.start_lat);
            const startLong = Number(req.body.start_long);
            const endLat = Number(req.body.end_lat);
            const endLong = Number(req.body.end_long);
            const riderName = req.body.rider_name;
            const driverName = req.body.driver_name;
            const driverVehicle = req.body.driver_vehicle;
    
            const rideData = {startLat, startLong, endLat, endLong, riderName, driverName, driverVehicle};

            const result =  await rideServiceInstance.createRide(rideData);

            return res.send(result);
        }catch(err) {
            logger.error('🔥 Error: %o', err);
            return res.send({
                errorCode: 'SERVER_ERROR',
                message: 'Unknown error'
            });
        }
    });

    route.get('/', validatePaging, async (req: Request, res: Response) => {
        const logger: winston.Logger = Container.get('Logger');
        try {
            const { pageNumber, qtyPerPage }: PagingQueryParams = req.query;
            const result = await rideServiceInstance.getRides({pageNumber, qtyPerPage});
            if (!result.length) {
                return res.send({
                    errorCode: 'RIDES_NOT_FOUND_ERROR',
                    message: 'Could not find any rides'
                });
            }
            return res.send(result);
        }catch(err) {
            logger.error('🔥 Error: %o', err);
            return res.send({
                errorCode: 'SERVER_ERROR',
                message: 'Unknown error'
            });
        } 
    });

    
};