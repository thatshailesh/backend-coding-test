import { Router } from 'express';
import ride from './routes/ride';

export default (): Router => {
    const app = Router();
    ride(app);

    return app;
};