import { Request, Response, NextFunction }  from 'express';
import { IValidationError } from '../../interfaces/IValidationError';
import { PagingQueryParams } from '../../interfaces/IPagingParams';

export default (req: Request, res: Response, next: NextFunction): Response<IValidationError> | void => {
    const { pageNumber, qtyPerPage }: PagingQueryParams = req.query;
    if (pageNumber && (Number.isNaN(Number(pageNumber)) || Number(pageNumber) <= 0)) {
        return res.send({
            errorCode: 'VALIDATION_ERROR',
            message: 'Query param pageNumber must be a positive integer or greater than 0'
        });
    }
    if (qtyPerPage && (Number.isNaN(Number(qtyPerPage)) || Number(qtyPerPage) <= 0)) {
        return res.send({
            errorCode: 'VALIDATION_ERROR',
            message: 'Query param qtyPerPage must be a positive integer or greater than 0'
        });
    }

    return next();
};