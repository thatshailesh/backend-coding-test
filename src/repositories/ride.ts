import SqlString from 'sqlstring';
import { Service, Inject } from 'typedi';

import { IRideRepository } from '../interfaces/IRideRepository';
import { IRide } from '../interfaces/IRide';
import { Database } from 'sqlite3';
import dbModel from '../models/db';
import winston from 'winston';

@Service()
export default class RideRepository implements IRideRepository {
    @Inject('Db') private db: Database;
    @Inject('Logger') private logger: winston.Logger;

    async saveRide(values: any[]): Promise<IRide[]> {
        this.logger.info('RideRepository: saveRide()');
        this.logger.debug('RideRepository: saveRide() -> date: %o', values);
        const result = await dbModel.dbRun(this.db, SqlString.format('INSERT INTO Rides(startLat, startLong, endLat, endLong, riderName, driverName, driverVehicle) VALUES (?, ?, ?, ?, ?, ?, ?)', values));
        return dbModel.dbAll(this.db, SqlString.format('SELECT * FROM Rides WHERE rideID = ?', result.lastID as any));
    }

    getRides(limit?: number, offset?: number): Promise<IRide[]> {
        this.logger.info('RideRepository: getRides()');
        let query = 'SELECT * FROM Rides';
        const values = [];
        if (typeof limit !== 'undefined' && typeof offset !== 'undefined' ) {
            this.logger.debug('RideRepository: getRides() -> limit: %o offset: %s', limit, offset);
            query += ' LIMIT ? OFFSET ?';
            values.push(limit, offset);
        }

        return dbModel.dbAll(this.db, SqlString.format(query, values));
    }

    getRideById(id: string): Promise<IRide[]> {
        this.logger.info('RideRepository: getRideById()');

        this.logger.debug('RideRepository: getRideById() -> id: %o', id);
        return dbModel.dbAll(this.db, SqlString.format('SELECT * FROM Rides WHERE rideID=?', id as any));
    }
}