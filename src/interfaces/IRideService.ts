import { IRide } from './IRide';
import { PagingQueryParams } from './IPagingParams';

export interface IRideService {
    createRide(ride: IRide): Promise<IRide[]>;
    getRides(pagingParams: PagingQueryParams): Promise<IRide[]>;
    getRideById(id: string): Promise<IRide[]>;
}