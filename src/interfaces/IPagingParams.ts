export interface PagingQueryParams {
    pageNumber?: string;
    qtyPerPage?: string;
}