export interface IValidationError{
    errCode: string;
    message: string;
}