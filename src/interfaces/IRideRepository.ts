import { IRide } from './IRide';

export interface IRideRepository {
    saveRide(values: any[]): Promise<IRide[]>;
    getRides(limit?: number, offset?: number): Promise<IRide[]>;
    getRideById(id: string): Promise<IRide[]>;
}