# Ride Management API
Api to get and create new rides
## Endpoints

### API health

```
URL: http://localhost:8010/health
Method: GET
Response: Healthy
```
### Create Ride


URL: `http://localhost:8010/rides`

Method: `POST`

Authentication:
- Username: `shailesh`
- password: `incorrect`


Body: 
```
{
    "rideID": {Number},
    "startLat": {Number},
    "startLong": {Number},
    "endLat":  {Number},
    "endLong":  {Number},
    "riderName": {String}
    "driverName": {String}
    "driverVehicle": {String}
    "created": {Date}
}
```
Constraints:
- Start latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively
- End latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively
- Rider name must be a non empty string
- Driver name must be a non empty string
- Driver Vehicle must be a non empty string

Response: 
```
[
  {
    "rideID": {Number},
    "startLat": {Number},
    "startLong": {Number},
    "endLat":  {Number},
    "endLong":  {Number},
    "riderName": {String}
    "driverName": {String}
    "driverVehicle": {String}
    "created": {Date}
  }
]
```
URL: `http://localhost:8010/rides?pageNumber={pageNumber}&qtyPerPage={qtyPerPage}`

Method: `GET`

Authentication:
- Username: `shailesh`
- password: `incorrect`


Query Params:
 - pageNumber: `optional` | `{Number}`
 - qtyPerPage: `optional` | optional


Response: 
```
[
  {
    "rideID": {Number},
    "startLat": {Number},
    "startLong": {Number},
    "endLat":  {Number},
    "endLong":  {Number},
    "riderName": {String}
    "driverName": {String}
    "driverVehicle": {String}
    "created": {Date}
  }
]
```

### Get Ride By Id


URL: `http://localhost:8010/rides/:id`

Method: `GET`

Authentication:
- Username: `shailesh`
- password: `incorrect`


Params:
 - id:  `{Number}`

Response: 
```
[
  {
    "rideID": {Number},
    "startLat": {Number},
    "startLong": {Number},
    "endLat":  {Number},
    "endLong":  {Number},
    "riderName": {String}
    "driverName": {String}
    "driverVehicle": {String}
    "created": {Date}
  }
]
```
