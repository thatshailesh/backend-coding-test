import 'reflect-metadata';
import sinon from 'sinon';
import Chance from 'chance';
import RideService from '../src/services/ride';
import { IRideService } from '../src/interfaces/IRideService';
import RideRepository from '../src/repositories/ride';
import { IRideRepository } from '../src/interfaces/IRideRepository';
import { Container } from 'typedi';
import { expect } from 'chai';

import dependencyInjector from '../src/loaders/dependencyInjector';
import dbModel from '../src/models/db';
import { IRide } from '../src/interfaces/IRide';

const chance = Chance();
describe('Ride Service Tests', () => {
    let rideService: IRideService;
    let rideRepo: IRideRepository;
    let saveRide: sinon.SinonSpy;
    let rideData: IRide;
    let getRideById: sinon.SinonSpy;
    let dbRunStub: sinon.SinonStub;
    let dbAllStub: sinon.SinonStub;
    let getRides: sinon.SinonSpy;

    const getRideData = () => {
        const startLat = chance.integer({ min: -90, max: 90});
        const startLong = chance.integer({ min: -180, max: 180});
        const endLat = chance.integer({ min: -90, max: 90 });
        const endLong = chance.integer({ min: -180, max: 180 });
        const riderName = chance.name();
        const driverName = chance.name();
        const driverVehicle = chance.string();

        return {startLat, startLong, endLat, endLong, riderName, driverName, driverVehicle};
    };

    const db = {};

    before(async () => {
        await dependencyInjector(db as any);

    });

    beforeEach(async () => {
        rideService = Container.get(RideService);
        rideRepo = Container.get(RideRepository);
        saveRide = sinon.spy(rideRepo, 'saveRide');
        getRideById = sinon.spy(rideRepo, 'getRideById');
        getRides = sinon.spy(rideRepo, 'getRides');
        
    });
    afterEach(async () => {
        saveRide.restore();
        getRideById.restore();
        dbRunStub.restore();
        dbAllStub.restore();
        getRides.restore();
    });

    it('should be able to save new ride', async () => {
        rideData = getRideData();
        const expectedResult = [{...rideData, rideID: 1}];
        dbRunStub = sinon.stub(dbModel, 'dbRun').returns(Promise.resolve({}) as any);
        dbAllStub = sinon.stub(dbModel, 'dbAll').returns(Promise.resolve(expectedResult));
        const result = await rideService.createRide(rideData);
        expect(saveRide.callCount).to.equal(1);
        expect(dbRunStub.calledOnce).to.equal(true);
        expect(dbAllStub.calledOnce).to.equal(true);
        expect(result).to.eql(expectedResult);
    });

    it('should be able to get ride by id', async () => {
        rideData = getRideData();
        const expectedResult = [{...rideData, rideID: 2}];
        dbRunStub = sinon.stub(dbModel, 'dbRun').returns(Promise.resolve({}) as any);
        dbAllStub = sinon.stub(dbModel, 'dbAll').returns(Promise.resolve(expectedResult));
        const result = await rideService.getRideById('2');
        expect(getRideById.callCount).to.equal(1);
        expect(dbAllStub.calledOnce).to.equal(true);
        expect(result).to.eql(expectedResult);
    });

    it('should be able to get all rides', async () => {
        const expectedResult = [];
        for (let i = 0; i < 3; i++) {
            expectedResult.push({...getRideData(), rideID: i + 1});
        }
        dbAllStub = sinon.stub(dbModel, 'dbAll').returns(Promise.resolve(expectedResult));
        const result = await rideService.getRides({});
        expect(getRides.callCount).to.equal(1);
        expect(dbAllStub.calledOnce).to.equal(true);
        expect(result).to.eql(expectedResult);
    });

    it('should be able to get paginated ride data', async () => {
        const expectedResult = [];
        const pagingParams = {
            pageNumber: '2',
            qtyPerPage: '3'
        };
        for (let i = 0; i < 10; i++) {
            expectedResult.push({...getRideData(), rideID: i + 1});
        }
        const limit = parseInt(pagingParams.qtyPerPage, 10);
        const offset = limit * (parseInt(pagingParams.pageNumber, 10) - 1);
        dbAllStub = sinon.stub(dbModel, 'dbAll').returns(Promise.resolve(expectedResult.slice(0, offset)));
        const result = await rideService.getRides(pagingParams);
        expect(getRides.callCount).to.equal(1);
        expect(getRides.withArgs(limit, offset).calledOnce).to.be.true;
        expect(dbAllStub.calledOnce).to.equal(true);
        expect(result).to.eql(expectedResult.slice(0, offset));
    });

});