/* eslint-disable @typescript-eslint/camelcase */
'use strict';
import request from 'supertest';
import Chance from 'chance';
import config from 'config';
import express from 'express';
import { expect } from 'chai';
import { verbose } from 'sqlite3';

const sqlite3 = verbose();
const db = new sqlite3.Database(':memory:');
const chance = Chance();

const username: string = config.get('auth.username');
const password: string = config.get('auth.password');

import { createRideTableSchema } from '../src/models/schemas';
import { IRide } from '../src/interfaces/IRide';
import expressLoader from '../src/loaders/express';
import dependencyInjector from '../src/loaders/dependencyInjector';
const app = express();

describe('API tests', () => {
    before(async () => {
        await createRideTableSchema(db);
        await dependencyInjector(db);
        await expressLoader(app);
        
    });

    describe('GET /health', () => {
        it('should return health', (done) => {
            request(app)
                .get('/health')
                .auth(username, password)
                .expect('Content-Type', /text/)
                .expect(200, done);
        });
    });

    describe('GET /rides no auth given', () => {
        it('should return not authorized error', (done) => {
            request(app)
                .get('/rides')
                .expect(401, done);
        });
    });

    describe('GET /rides without data', () => {
        it('should return error when rides not available', (done) => {
            request(app)
                .get('/rides')
                .auth(username, password)
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200, {
                    errorCode: 'RIDES_NOT_FOUND_ERROR',
                    message: 'Could not find any rides',
                }, done);
        });

        it('should return error upon single ride request', (done) => {
            request(app)
                .get('/rides/44')
                .auth(username, password)
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200, {
                    errorCode: 'RIDES_NOT_FOUND_ERROR',
                    message: 'Could not find any rides',
                }, done);
        });
    });

    describe('GET /rides with sample data', () => {
        const totalRides = 10;
        const insertMockDataList: any[] = [];
        let rideData: IRide[] = [];
        before((done) => {
            const insetMockData = async () => {
                const startLat = chance.integer({ min: -90, max: 90});
                const startLong = chance.integer({ min: -180, max: 180});
                const endLat = chance.integer({ min: -90, max: 90 });
                const endLong = chance.integer({ min: -180, max: 180 });
                const riderName = chance.name();
                const driverName = chance.name();
                const driverVehicle = chance.string();
                const values = [startLat, startLong, endLat, endLong, riderName, driverName, driverVehicle];
                const insertQuery = 'INSERT INTO Rides \
                (startLat, startLong, endLat, endLong, riderName, driverName, driverVehicle) \
                VALUES (?, ?, ?, ?, ?, ?, ?)';
                return new Promise((resolve, reject) => {
                    db.run(insertQuery, values, (err) => {
                        if (err) {
                            reject();
                        }else {
                            resolve({
                                startLat,
                                startLong,
                                endLat,
                                endLong,
                                riderName,
                                driverName,
                                driverVehicle
                            });
                        }
                    });
                });
            };
            db.serialize(async () => {
                for (let i = 0; i < totalRides; i++) {
                    insertMockDataList.push(insetMockData());
                }
                rideData = await Promise.all(insertMockDataList);
                done();
            });
        });
        it('should be able to get all rides available', (done) => {

            request(app)
                .get('/rides')
                .auth(username, password)
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    for (let i = 0; i < rideData.length; i++) {
                        const isoDate = new Date().toISOString().split('.')[0];
                        rideData[i].rideID = i + 1;
                        rideData[i].created = isoDate.split('T').join(' ');
                    }
                    expect(res.body).to.eql(rideData);
                    done();
                });
        });

        it('should return data upon single ride request', (done) => {
            const expectedResult = [rideData[0]];
            request(app)
                .get('/rides/1')
                .auth(username, password)
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200, expectedResult, done);
        });

        it('should be able to get 2 rides for first page', (done) => {
            const expectedResult = rideData.slice(0, 2);
            
            request(app)
                .get('/rides?pageNumber=1&qtyPerPage=2')
                .auth(username, password)
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200, expectedResult, done);
        });

        it('should be able to get two rides second page', (done) => {
            const expectedResult = rideData.slice(2, 4);
            request(app)
                .get('/rides?pageNumber=2&qtyPerPage=2')
                .auth(username, password)
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200, expectedResult, done);
        });

        it('should fail for invalid page number query param', (done) => {
            request(app)
                .get('/rides?pageNumber=-2&qtyPerPage=2')
                .auth(username, password)
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200, {
                    errorCode: 'VALIDATION_ERROR',
                    message: 'Query param pageNumber must be a positive integer or greater than 0'
                }, done);
        });

        it('should fail for invalid quantity per page query param', (done) => {
            request(app)
                .get('/rides?pageNumber=1&qtyPerPage=-2')
                .auth(username, password)
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200, {
                    errorCode: 'VALIDATION_ERROR',
                    message: 'Query param qtyPerPage must be a positive integer or greater than 0'
                }, done);
        });
    });

    describe('POST /rides', () => {
        it('should be able to create rides', (done) => {
            const isoDate = new Date().toISOString().split('.')[0];
            const expectedResult: IRide[] = [{
                startLat: 2,
                startLong: 4,
                endLat: 5,
                endLong: 1,
                riderName: 'Rider A',
                driverName: 'Driver D',
                driverVehicle: 'Vehicle V',
                created: isoDate.split('T').join(' '),
                rideID: 1
            }];
            request(app)
                .post('/rides')
                .auth(username, password)
                .send({
                    start_lat: 2,
                    start_long: 4,
                    end_lat: 5,
                    end_long: 1,
                    rider_name: 'Rider A',
                    driver_name: 'Driver D',
                    driver_vehicle: 'Vehicle V'
                })
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    expect(res.body).to.be.an('array');
                    expect(res.body[0]).to.have.property('rideID');
                    expectedResult[0].rideID = res.body[0].rideID;
                    expect(res.body).to.eql(expectedResult);
                    done();
                });
        });

        it('should fail when start location is invalid', (done) => {
            request(app)
                .post('/rides')
                .auth(username, password)
                .send({
                    start_lat: -122,
                    start_long: 4,
                    end_lat: 5,
                    end_long: 1,
                    rider_name: 'Rider A',
                    driver_name: 'Driver D',
                    driver_vehicle: 'Vehicle V'
                })
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200, {
                    errorCode: 'VALIDATION_ERROR',
                    message: 'Start latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively'
                }, done);
        });

        it('should fail when end location is invalid', (done) => {
            request(app)
                .post('/rides')
                .auth(username, password)
                .send({
                    start_lat: 2,
                    start_long: 4,
                    end_lat: 95,
                    end_long: 1,
                    rider_name: 'Rider A',
                    driver_name: 'Driver D',
                    driver_vehicle: 'Vehicle V'
                })
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200, {
                    errorCode: 'VALIDATION_ERROR',
                    message: 'End latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively'
                }, done);
        });

        it('should fail when rider name is invalid', (done) => {
            request(app)
                .post('/rides')
                .auth(username, password)
                .send({
                    start_lat: 2,
                    start_long: 4,
                    end_lat: 5,
                    end_long: 1,
                    rider_name: '',
                    driver_name: 'Driver D',
                    driver_vehicle: 'Vehicle V'
                })
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200, {
                    errorCode: 'VALIDATION_ERROR',
                    message: 'Rider name must be a non empty string'
                }, done);
        });

        it('should fail when driver name is invalid', (done) => {
            request(app)
                .post('/rides')
                .auth(username, password)
                .send({
                    start_lat: 2,
                    start_long: 4,
                    end_lat: 5,
                    end_long: 1,
                    rider_name: 'Rider A',
                    driver_name: '',
                    driver_vehicle: 'Vehicle V'
                })
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200, {
                    errorCode: 'VALIDATION_ERROR',
                    message: 'Driver name must be a non empty string'
                }, done);
        });

        it('should fail when driver vehicle is invalid', (done) => {
            request(app)
                .post('/rides')
                .auth(username, password)
                .send({
                    start_lat: 2,
                    start_long: 4,
                    end_lat: 5,
                    end_long: 1,
                    rider_name: 'Rider A',
                    driver_name: 'Driver D',
                    driver_vehicle: ''
                })
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200, {
                    errorCode: 'VALIDATION_ERROR',
                    message: 'Driver vehicle must be a non empty string'
                }, done);
        });
    });
});